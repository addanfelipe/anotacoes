/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  // Button,
} from 'react-native';

import {
  Container,
  Header,
  Content,
  Badge,
  Text,
  Icon,
  Card,
  CardItem,
  Body,
  Left,
  Right,
  View,
  Button,
  Item,
  Input,
  Textarea,
  Form,
  Title,
  Subtitle,
} from 'native-base';

import {
  Col,
  Row,
  Grid,
} from 'react-native-easy-grid';

import models from './src/models'
import utils from './src/utils'
import ComponentAnotacao from './src/components/ComponentAnotacao'
import ComponentAppCtrl from './src/components/ComponentAppCtrl'

let SQLite = require('react-native-sqlite-storage')

export default class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      listAnotacaoBack: [],
      anotacaoDad: new models.Anotacao(),
      listAnotacaoChild: [],
      textInsertText: '',
      anotacaoSelected: null
    }

    this.insertAnotacao = this.insertAnotacao.bind(this)
    this.showAnotacoesSon = this.showAnotacoesSon.bind(this)
    this.changeDad = this.changeDad.bind(this)
    this.backDad = this.backDad.bind(this)
    this.showCaseAnotacaoSelected = this.showCaseAnotacaoSelected.bind(this)

    this.init()
  }

  async init() {
    let db = SQLite.openDatabase({ name: 'test.db', createFromLocation: '~db.sqlite3' })
    this.ctrl = new ComponentAppCtrl(db)
    this.ctrl.init(async () => {
      this.changeDad(await models.Anotacao.objects.getHome(), false, true)
    })
  }

  async changeDad(anotacaoNewDad, isBack = false, isFirstAnotacao = false) {
    await this.ctrl.changeDad(anotacaoNewDad, isBack, isFirstAnotacao)
    this.setState({
      listAnotacaoChild: this.ctrl.state.listAnotacaoChild,
      anotacaoDad: this.ctrl.state.anotacaoDad,
      listAnotacaoBack: this.ctrl.state.listAnotacaoBack
    })
  }

  async backDad() {
    await this.ctrl.backDad()
    this.setState({
      listAnotacaoChild: this.ctrl.state.listAnotacaoChild,
      anotacaoDad: this.ctrl.state.anotacaoDad,
      listAnotacaoBack: this.ctrl.state.listAnotacaoBack
    })
  }

  async insertAnotacao() {
    await this.ctrl.insertAnotacao(this.state.textInsertText)
    this.setState({ listAnotacaoChild: this.ctrl.state.listAnotacaoChild, textInsertText: '' })
  }

  showAnotacoesSon() {
    let listAnotacaoChild = this.state.listAnotacaoChild
    let listShow = []
    for (let anotacao of listAnotacaoChild) {
      listShow.push(
        <ComponentAnotacao key={anotacao.id}
          onPressTextContent={() => this.changeDad(anotacao)}
          text={anotacao.text}>
        </ComponentAnotacao>
      )
    }

    return (
      <Content>
        <ComponentAnotacao key={this.state.anotacaoDad.id}
          onPressTextContent={() => this.changeDad(this.state.anotacaoDad)}
          text={this.state.anotacaoDad.text}
          styleView={{ backgroundColor: 'lightgray' }}
          styleText={{}}
          styleTextContent={{ fontSize: 20, color: 'black' }}>
        </ComponentAnotacao>
        {listShow}
      </Content>
    )
  }

  showSubtitle(text) {
    return text ? text.split('\n').join(' ').substring(0, 20) : ''
  }

  showCaseAnotacaoSelected(isAnotacaoSelected) {
    if (isAnotacaoSelected) {
      return (
        <Button transparent onPress={() => { }}>
          <Icon type="MaterialIcons" name='delete' />
        </Button>
      )
    } else {
      return (<View></View>)
    }
  }

  render() {
    return (
      <Container>
        <Header style={{ backgroundColor: 'dodgerblue' }}>
          <Left>
            <Button transparent onPress={this.backDad}>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title>Anotações</Title>
            <Subtitle>{this.showSubtitle(this.state.anotacaoDad.text)}</Subtitle>
          </Body>
          <Right>
            {this.showCaseAnotacaoSelected(this.state.anotacaoSelected !== null)}
          </Right>
        </Header>
        <Content>
          {this.showAnotacoesSon()}
        </Content>
        <Form>
          <Item rounded style={{ margin: 5, marginLeft: 10, marginRight: 10 }}>
            <Textarea style={{ width: '87%' }} placeholder="Digite a anotação"
              rowSpan={2}
              onChangeText={(textInsertText) => this.setState({ textInsertText })}
              value={this.state.textInsertText}
            />
            <Button transparent style={{ width: '13%' }} info
              onPress={this.insertAnotacao}>
              <Icon name='send' style={{ color: 'dodgerblue' }} />
            </Button>
          </Item>
        </Form>
      </Container>
    )
  }

}

const styles = StyleSheet.create({
  // container: {
  //   flex: 1,
  //   justifyContent: 'center',
  //   alignItems: 'center',
  //   backgroundColor: '#F5FCFF',
  // },
  // welcome: {
  //   fontSize: 20,
  //   textAlign: 'center',
  //   margin: 10,
  // },
  // instructions: {
  //   textAlign: 'center',
  //   color: '#333333',
  //   marginBottom: 5,
  // },
});
