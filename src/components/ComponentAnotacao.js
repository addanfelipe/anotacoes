import React, { Component } from 'react';
import {
    StyleSheet,
    // Button,
} from 'react-native';

import {
    Container,
    Header,
    Content,
    Badge,
    Text,
    Icon,
    Card,
    CardItem,
    Body,
    Left,
    Right,
    View,
    Button,
    Item,
    Input,
    Textarea,
    Form,
    Title,
    Subtitle,
} from 'native-base';

import {
    Col,
    Row,
    Grid,
} from 'react-native-easy-grid';

export default class ComponentAnotacao extends Component {

    constructor(props) {
        super(props)

        this.state = {
        }

    }

    render() {

        let styles = {
            view: {
                fontWeight: 'bold',
                padding: 5,
                paddingLeft: 10,
                paddingRight: 10,
                marginTop: 5,
                marginLeft: 10,
                marginRight: 10,
                alignItems: 'flex-start',
                borderRadius: 10,
                // borderLeftWidth: 3,
                // borderBottomWidth: 2,
                // borderColor: 'dodgerblue',
                backgroundColor: 'dodgerblue',
            },
            textColor: {
                color: 'white'
            },
            textContent: {
                fontSize: 14
            },
            textFooterLeft: {
                alignSelf: 'flex-end',
                fontSize: 10
            },
            styleView: this.props.styleView ? this.props.styleView : {},
            styleTextContent: this.props.styleTextContent ? this.props.styleTextContent : {},
            styleText: this.props.styleText ? this.props.styleText : {},
        }

        return (
            <View style={[styles.view, styles.styleView]}>
                <Text style={[styles.textColor, styles.textContent, styles.styleTextContent]}
                    onPress={this.props.onPressTextContent}
                >
                    {this.props.text}
                </Text>
                <View>
                    {this.props.children}
                </View>
                {/* <Text style={[styles.textColor, styles.textFooterLeft]}>
                    {this.props.textFooterLeft}
                </Text> */}
            </View>
        )
    }
}