import models from '../models'

export default class ComponentAppCtrl {
    constructor(db) {
        this.db = db
        this.state = {
            listAnotacaoBack: [],
            anotacaoDad: new models.Anotacao(),
            listAnotacaoChild: [],
            textInsertText: '',
            anotacaoSelected: null
        }

    }

    async init(callb) {
        await models.migrations(this.db)
        await callb()
    }

    async changeDad(anotacaoNewDad, isBack = false, isFirstAnotacao = false) {
        let listAnotacaoBack = this.state.listAnotacaoBack
        let isNotBackInFirstDad = !(this.state.listAnotacaoBack.length === 0 && isBack)
        let isNotNewDadEqualsOldDad = this.state.anotacaoDad.id !== anotacaoNewDad.id

        if (isNotBackInFirstDad && isNotNewDadEqualsOldDad) {
            let listAnotacaoChild = await anotacaoNewDad.allChild()

            if (isBack) {
                listAnotacaoBack = listAnotacaoBack.slice(0, listAnotacaoBack.length) // remove o ultimo elemento
            } else if (!isFirstAnotacao) {
                listAnotacaoBack.push(this.state.anotacaoDad) // adiciona na fila
            }

            this.state.listAnotacaoChild = listAnotacaoChild
            this.state.anotacaoDad = anotacaoNewDad
            this.state.listAnotacaoBack = listAnotacaoBack
        }
    }

    async backDad() {
        if (this.state.listAnotacaoBack.length !== 0) {
            await this.changeDad(this.state.listAnotacaoBack[0], true)
        }
    }

    async insertAnotacao(textInsertText) {
        let anotacaoDad = this.state.anotacaoDad
        let listAnotacaoChild = this.state.listAnotacaoChild

        let anotacaoInsert = new models.Anotacao({ text: textInsertText })
        await anotacaoInsert.create()
        await anotacaoDad.pushChild(anotacaoInsert)

        listAnotacaoChild.push(anotacaoInsert)
        this.state.listAnotacaoChild = listAnotacaoChild
    }

}