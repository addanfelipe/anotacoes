// let SQLite = require('react-native-sqlite-storage')
// let db = SQLite.openDatabase({ name: 'test.db', createFromLocation: '~db.sqlite3' })
let db = null

let executeSql = async (sql, args = []) => {
    return await new Promise(await (async (resolve, reject) => {
        if (db) {
            await db.transaction(await (async (tx) => {
                await tx.executeSql(sql, args, await (async (tx, results) => {
                    resolve(results)
                }), await (async (err) => {
                    reject(err)
                }))
            }))
        } else {
            resolve('db = null')
        }
    }))
}

class AnotacaoManager {

    static migrations = async (tx) => {
        await executeSql(`
            CREATE TABLE IF NOT EXISTS anotacao (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                text TEXT NOT NULL,
                audio TEXT,
                isHome INTEGER,
                isRascunho INTEGER
            );
        `)
        await executeSql(`
            CREATE TABLE IF NOT EXISTS anotacao_dad_child (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                anotacao_dad_id INTEGER,
                anotacao_child_id INTEGER
            );
        `)
    }

    create = async (anotacao) => {
        let results = await executeSql(
            `INSERT INTO anotacao (text, isHome, isRascunho) VALUES (?, ?, ?);`,
            [anotacao.text, anotacao.isHome, anotacao.isRascunho]
        )
        anotacao.id = results.insertId
        return anotacao
    }

    getHome = async () => {
        let results = await executeSql(`SELECT a.* FROM anotacao as a WHERE a.isHome = 1 LIMIT 1`)
        if (results.rows.length === 0) {
            throw {
                message: 'Anotação home não existe',
                name: 'Anotação home não existe'
            }
        } else {
            return new Anotacao(results.rows.item(0))
        }
    }

    static __parseObject = (results) => {
        let listResults = []
        for (let index = 0; index < results.rows.length; index++) {
            listResults.push(new Anotacao(results.rows.item(index)))
        }
        return listResults
    }

    all = async () => {
        let results = await executeSql(`SELECT a.* FROM anotacao as a`)
        return AnotacaoManager.__parseObject(results)
    }

    createDadChild = async (anotacaoDad, anotacaoChild) => {
        return await executeSql(
            `INSERT INTO anotacao_dad_child (anotacao_dad_id, anotacao_child_id) VALUES (?, ?);`,
            [anotacaoDad.id, anotacaoChild.id]
        )
    }

    filterChildOwnedbyDad = async (anotacaoDad) => {
        let results = await executeSql(`
            SELECT a.* FROM anotacao as a, anotacao_dad_child as adc
                WHERE a.id = adc.anotacao_child_id
                    AND adc.anotacao_dad_id = ? ORDER BY id ASC
            `, [anotacaoDad.id]
        )
        return AnotacaoManager.__parseObject(results)
    }

}


class Anotacao {

    constructor(args = {}) {
        this.text = args.text === undefined ? null : args.text
        this.audio = args.audio === undefined ? null : args.audio
        this.isHome = args.isHome === undefined ? false : args.isHome
        this.isRascunho = args.isRascunho === undefined ? false : args.isRascunho
        this.id = args.id === undefined ? null : args.id
    }

    static objects = new AnotacaoManager()

    async create() {
        await Anotacao.objects.create(this)
    }

    async pushChild(anotacaoChild) {
        return await Anotacao.objects.createDadChild(this, anotacaoChild)
    }

    async allChild() {
        return await Anotacao.objects.filterChildOwnedbyDad(this)
    }

    static migrations = async () => {
        await AnotacaoManager.migrations()
        try {
            await Anotacao.objects.getHome()
        } catch (error) {
            let anotacaoHome = new Anotacao({ text: 'Home', isHome: 1 })
            await anotacaoHome.create()
            let anotacaoRascunho = new Anotacao({ text: 'Rascunho', isRascunho: 1 })
            await anotacaoRascunho.create()
            await anotacaoHome.pushChild(anotacaoRascunho)
        }
    }
}


let migrations = async (dbArg) => {
    db = dbArg
    await Anotacao.migrations()
}

export default {
    migrations,
    Anotacao,
}