import Moment from 'moment';

let alertJSON = (text) => {
    alert(JSON.stringify(text))
}

let formatDateTime = (dateObj) => {
    return Moment(dateObj).format('DD/MM/YYYY HH:mm')
}

export default {
    alertJSON,
    formatDateTime,
}